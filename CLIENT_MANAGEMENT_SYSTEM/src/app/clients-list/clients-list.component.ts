import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { OrderService } from '../services/order.service';

@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.scss']
})
export class ClientsListComponent implements OnInit {

  constructor(public authService: AuthService, private router: Router, public orderService: OrderService) { 
    console.log(localStorage.getItem('users'))
  }

  ngOnInit(): void {
  }
  navigate(data) {
    console.log(data);
    this.router.navigate(['landing/client-details', data.id]); 
  }
  clientList = 
  [{"id":1, "clientName":"BOA","companyName":"PWC","title":"Finance","department":"Finance","email":"boa@gmail.com","phoneNumber":8789787891,"address":"5th Floor, Ulsoor Road, Bangalore","address2":"Ulsoor Road, Bangalore","city":"Bangalore","state":"Karnataka","zip":"560042","country":"India",},
  {"id":2,"clientName":"Samsung","companyName":"Qualcomm","title":"Test Title","department":"Test Department","email":"test@gmail.com","phoneNumber":878978789,"address":"Test-address","address2":"Test-address-2","city":"Bangalore","state":"Karnataka","zip":"56789","country":"India",},
  {"id":3,"clientName":"Qualcomm","companyName":"Global Edge","title":"Test Title","department":"Test Department","email":"test@gmail.com","phoneNumber":878978789,"address":"Test-address","address2":"Test-address-2","city":"Bangalore","state":"Karnataka","zip":"56789","country":"India",},
  {"id":4,"clientName":"Dell","companyName":"Accenture","title":"Test Title","department":"Test Department","email":"test@gmail.com","phoneNumber":878978789,"address":"Test-address","address2":"Test-address-2","city":"Bangalore","state":"Karnataka","zip":"56789", "country":"India",},
  {"id":5,"clientName":"Test Client","companyName":"Solutions","title":"Taxation","department":"Test Department","email":"test@gmail.com","phoneNumber":878978789,"address":"Test-address","address2":"Test-address-2","city":"Bangalore","state":"Karnataka","zip":"56789","country":"India",},
  {"id":6,"clientName":"Qualcomm","companyName":"F5 Networks","title":"Test Title","department":"Test Department","email":"test@gmail.com","phoneNumber":878978789,"address":"Test-address","address2":"Test-address-2","city":"Bangalore","state":"Karnataka","zip":"56789","country":"India",},
  {"id":7,"clientName":"Test-Client-1","companyName":"Test-Company-1","title":"Test-title-1","department":"test-dept-1","email":"test1@gmail.com","phoneNumber":8976787867,"address":"test-address","address2":"test-address-2","city":"Bangalore","state":"Karnataka","zip":"56789","country":"India",},
  {"id":8,"clientName":"Test-Client-2","companyName":"Test-Company-2","title":"Test-title-2","department":"test-dept-2","email":"test2@gmail.com","phoneNumber":8976787868,"address":"test-address","address2":"test-address-2","city":"Bangalore","state":"Karnataka","zip":"56789","country":"India",},
  {"id":9,"clientName":"Test-Client-3","companyName":"Test-Company-3","title":"Test-title-3","department":"test-dept-3","email":"test3@gmail.com","phoneNumber":8976787869,"address":"test-address","address2":"test-address-2","city":"Bangalore","state":"Karnataka","zip":"56789","country":"India",},
  {"id":10,"clientName":"Test-Client-4","companyName":"Test-Company-4","title":"Test-title-4","department":"test-dept-4","email":"test4@gmail.com","phoneNumber":8906787867,"address":"test-address","address2":"test-address-2","city":"Bangalore","state":"Karnataka","zip":"56789","country":"India",},
  {"id":11,"clientName":"Test-Client-5","companyName":"Test-Company-5","title":"Test-title-5","department":"test-dept-5","email":"test5@gmail.com","phoneNumber":8976787861,"address":"test-address","address2":"test-address-2","city":"Bangalore","state":"Karnataka","zip":"56789","country":"India",}]

    deleteClient(id) {
      this.clientList.forEach( (item, index) => {
        if(item.id === id) this.clientList.splice(index,1);
      });
      console.log(this.clientList);
}
    }

