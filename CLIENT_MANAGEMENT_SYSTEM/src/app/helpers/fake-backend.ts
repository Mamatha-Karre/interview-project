import { Http, BaseRequestOptions, Response, ResponseOptions, RequestMethod } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';
 let token;
 let users = JSON.parse(localStorage.getItem('users')) || [];
export function fakeBackendFactory(backend: MockBackend, options: BaseRequestOptions) {
    backend.connections.subscribe((connection: MockConnection) => {
      let data = JSON.parse(connection.request.getBody())
      if(data.email === 'pwcAdmin@gmail.com') {
        token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IlBXQyBBZG1pbiIsImFkbWluIjp0cnVlLCJhdWRpdG9yIjpmYWxzZX0.rCvx4IM8WxpNGCM-jQ5KlQzvsff7hz31uyneEhlm9bI';
      } else if (data.email === 'pwcClient@gmail.com') {
        token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IlBXQyBDbGllbnQiLCJhZG1pbiI6ZmFsc2UsImF1ZGl0b3IiOmZhbHNlfQ.7JnkB1NYdjGeUbJa4LEWTN_C7kwld2PYcvMoYC6ABLE';
      } else {
        token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IlBXQyBBdWRpdG9yIiwiYWRtaW4iOmZhbHNlLCJhdWRpdG9yIjp0cnVlfQ.6sMXDVjlZKkHODWtI65usVbGqeqxHq5sjT4bCxMhji0'
      }
      
      // We are using the setTimeout() function to simulate an asynchronous call 
      // to the server that takes 1 second. 
      setTimeout(() => {
        //
        // Fake implementation of /api/authenticate
        //
        if (connection.request.url.endsWith('/api/authenticate') && 
            connection.request.method === RequestMethod.Post) {
            let body = JSON.parse(connection.request.getBody());

            if ((body.email === 'pwcAdmin@gmail.com' && body.password === 'admin@123') || 
            (body.email === 'pwcClient@gmail.com' && body.password === 'client@123') ||
            (body.email === 'pwcAuditor@gmail.com' && body.password === 'auditor@123')) {
              connection.mockRespond(new Response(
                new ResponseOptions({ 
                  status: 200, 
                  body: { token: token }
                })
              ));
            } else {
              connection.mockRespond(new Response(
                new ResponseOptions({ status: 200 })
              ));
            }
        }
        


        // 
        // Fake implementation of /api/clients
        //
        if (connection.request.url.endsWith('/api/clients') && connection.request.method === RequestMethod.Get) {
            if (connection.request.headers.get('Authorization') === 'Bearer ' + token) {
                connection.mockRespond(new Response(
                    new ResponseOptions({ status: 200, body: [1, 2, 3] })
                ));
            } else {
                connection.mockRespond(new Response(
                    new ResponseOptions({ status: 401 })
                ));
            }
        }  
        if (connection.request.url.endsWith('/api/register') && 
        connection.request.method === RequestMethod.Post) {
        let body = JSON.parse(connection.request.getBody());   
        const user = body
        if (users.find(x => x.username === user.username)) {
          return throwError({ error: { 'Username "' : user.username + '" is already taken' } });
      }
      debugger

      user.id = users.length ? Math.max(...users.map(x => x.id)) + 1 : 1;
      users.push(user);
      localStorage.setItem('users', JSON.stringify(users));
      console.log(localStorage.getItem('users'))

      return of(new HttpResponse({ status: 200, body }))
        }

      }, 1000);
    });
 
    return new Http(backend, options);
   
}
 
export let fakeBackendProvider = {
    provide: Http,
    useFactory: fakeBackendFactory,
    deps: [MockBackend, BaseRequestOptions]
};