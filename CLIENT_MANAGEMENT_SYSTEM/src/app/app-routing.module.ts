import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { LandingComponent } from './landing/landing.component';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { RegisterComponent } from './register/register.component';
import { ClientDetailsComponent } from './client-details/client-details.component';
import { AuthGuard } from './auth-guard.service';
import { AdminAuthGuard } from './admin-auth-guard.service';
import { auditorAuthGuard } from './auditor-auth-guard.service';
import { NoAccessComponent } from './no-access/no-access.component';


const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'landing', component: LandingComponent, canActivate: [AuthGuard], children:[
    {path: '', component: ClientsListComponent, canActivate: [AdminAuthGuard]},
    {path: 'client-list', component: ClientsListComponent, canActivate: [AdminAuthGuard]},
    {path: 'add-client', component: RegisterComponent, canActivate: [auditorAuthGuard]},
    {path: 'client-details/:id', component: ClientDetailsComponent}
  ]},
  { path: 'no-access', component: NoAccessComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
