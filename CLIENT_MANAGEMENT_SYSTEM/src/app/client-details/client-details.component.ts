import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { OrderService } from '../services/order.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-client-details',
  templateUrl: './client-details.component.html',
  styleUrls: ['./client-details.component.scss']
})
export class ClientDetailsComponent implements OnInit {
  registerForm: FormGroup;
  filteredData;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private orderService: OrderService,
    private route: ActivatedRoute,
    public authService: AuthService
) {
let value = this.route.snapshot.paramMap.get('id');
if(value) {
    this.filteredData =  this.orderService.clientList
                                .filter((item) => item.id === parseInt(value)); 
} else {
 this.filteredData = this.orderService.clientList[1];
}
}

ngOnInit() {
    this.registerForm = this.formBuilder.group({
        clientName: ['', Validators.required],
        companyName: ['', Validators.required],
        title: ['', Validators.required],
        department: ['', [Validators.required]],
        email: ['',[Validators.required]],
        phoneNumber: ['',[Validators.required, Validators.minLength, Validators.maxLength]],
        address: ['',[Validators.required]],
        address2: ['',[Validators.required]],
        city: ['',[Validators.required]],
        state: ['',[Validators.required]],
        zip: ['',[Validators.required]],
        country: ['',[Validators.required]]
    });
    this.registerForm.setValue({
      clientName: this.filteredData[0].clientName,
      companyName:this.filteredData[0].companyName,
      title: this.filteredData[0].title,
      department: this.filteredData[0].department,
      email: this.filteredData[0].email,
      phoneNumber: this.filteredData[0].phoneNumber,
      address: this.filteredData[0].address,
      address2: this.filteredData[0].address2,
      city: this.filteredData[0].city,
      state: this.filteredData[0].state,
      zip: this.filteredData[0].zip,
      country: this.filteredData[0].country
    });
}

// convenience getter for easy access to form fields
get f() { return this.registerForm.controls; }

goback() {
  this.router.navigate(['landing/client-list'])
}
}
