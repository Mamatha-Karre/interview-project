import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
      invalidLogin: boolean; 

      constructor(
        private router: Router, 
        private authService: AuthService) { }
        ngOnInit(): void {
        }
        onSubmit(credentials) {
        this.authService.login(credentials)
          .subscribe(result => { 
            if (result) {
             if (this.authService.currentUser.admin || this.authService.currentUser.auditor) {
                this.router.navigate(['landing']);
              } else this.router.navigate(['landing/client-details', 1]); 
            }
           
            else  
              this.invalidLogin = true; 
          });
      }
}
