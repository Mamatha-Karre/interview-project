import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { OrderService } from '../services/order.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
    loading = false;
    submitted = false;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private orderService: OrderService
    ) {

    }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            clientName: ['', Validators.required],
            companyName: ['', Validators.required],
            title: ['', Validators.required],
            department: ['', [Validators.required]],
            email: ['',[Validators.required]],
            phoneNumber: ['',[Validators.required, Validators.minLength, Validators.maxLength]],
            address: ['',[Validators.required]],
            address2: ['',[Validators.required]],
            city: ['',[Validators.required]],
            state: ['',[Validators.required]],
            zip: ['',[Validators.required]],
            country: ['',[Validators.required]]
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }

    onSubmit() {
      console.log(this.registerForm);
        this.submitted = true;
        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

      //  this.loading = true;
        this.orderService.register(this.registerForm.value)
        .subscribe(result => { 
        // this.userService.register(this.registerForm.value)
        //     .pipe(first())
        //     .subscribe(
        //         data => {
        //             this.alertService.success('Registration successful', true);
        //             this.router.navigate(['landing/client-list']);
        //         },
        //         error => {
        //             this.alertService.error(error);
        //             this.loading = false;
        //         });
    });
  }
 goback() {
   this.router.navigate(['landing/client-list'])
 }
}
