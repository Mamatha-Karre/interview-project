import { Injectable } from '@angular/core';
import { AuthHttp } from "angular2-jwt/angular2-jwt";
import { Observable, Subject } from 'rxjs';

@Injectable()
export class OrderService {
  constructor(private authHttp: AuthHttp) {
    
  }
  clientList = 
  [{"id":1, "clientName":"BOA","companyName":"PWC","title":"Finance","department":"Finance","email":"boa@gmail.com","phoneNumber":8789787891,"address":"5th Floor, Ulsoor Road, Bangalore","address2":"Ulsoor Road, Bangalore","city":"Bangalore","state":"Karnataka","zip":"560042","country":"India",},
  {"id":2,"clientName":"Samsung","companyName":"Qualcomm","title":"Test Title","department":"Test Department","email":"test@gmail.com","phoneNumber":878978789,"address":"Test-address","address2":"Test-address-2","city":"Bangalore","state":"Karnataka","zip":"56789","country":"India",},
  {"id":3,"clientName":"Qualcomm","companyName":"Global Edge","title":"Test Title","department":"Test Department","email":"test@gmail.com","phoneNumber":878978789,"address":"Test-address","address2":"Test-address-2","city":"Bangalore","state":"Karnataka","zip":"56789","country":"India",},
  {"id":4,"clientName":"Dell","companyName":"Accenture","title":"Test Title","department":"Test Department","email":"test@gmail.com","phoneNumber":878978789,"address":"Test-address","address2":"Test-address-2","city":"Bangalore","state":"Karnataka","zip":"56789", "country":"India",},
  {"id":5,"clientName":"Test Client","companyName":"Solutions","title":"Taxation","department":"Test Department","email":"test@gmail.com","phoneNumber":878978789,"address":"Test-address","address2":"Test-address-2","city":"Bangalore","state":"Karnataka","zip":"56789","country":"India",},
  {"id":6,"clientName":"Qualcomm","companyName":"F5 Networks","title":"Test Title","department":"Test Department","email":"test@gmail.com","phoneNumber":878978789,"address":"Test-address","address2":"Test-address-2","city":"Bangalore","state":"Karnataka","zip":"56789","country":"India",},
  {"id":7,"clientName":"Test-Client-1","companyName":"Test-Company-1","title":"Test-title-1","department":"test-dept-1","email":"test1@gmail.com","phoneNumber":8976787867,"address":"test-address","address2":"test-address-2","city":"Bangalore","state":"Karnataka","zip":"56789","country":"India",},
  {"id":8,"clientName":"Test-Client-2","companyName":"Test-Company-2","title":"Test-title-2","department":"test-dept-2","email":"test2@gmail.com","phoneNumber":8976787868,"address":"test-address","address2":"test-address-2","city":"Bangalore","state":"Karnataka","zip":"56789","country":"India",},
  {"id":9,"clientName":"Test-Client-3","companyName":"Test-Company-3","title":"Test-title-3","department":"test-dept-3","email":"test3@gmail.com","phoneNumber":8976787869,"address":"test-address","address2":"test-address-2","city":"Bangalore","state":"Karnataka","zip":"56789","country":"India",},
  {"id":10,"clientName":"Test-Client-4","companyName":"Test-Company-4","title":"Test-title-4","department":"test-dept-4","email":"test4@gmail.com","phoneNumber":8906787867,"address":"test-address","address2":"test-address-2","city":"Bangalore","state":"Karnataka","zip":"56789","country":"India",},
  {"id":11,"clientName":"Test-Client-5","companyName":"Test-Company-5","title":"Test-title-5","department":"test-dept-5","email":"test5@gmail.com","phoneNumber":8976787861,"address":"test-address","address2":"test-address-2","city":"Bangalore","state":"Karnataka","zip":"56789","country":"India",}]


  getOrders() { 
    return this.authHttp.get('/api/clients')
      .map(response => response.json());
  }
  register(user) {
    return this.authHttp.post('/api/register', JSON.stringify(user))
    .map(response => {
      let result = response.json();
});
  }
}
